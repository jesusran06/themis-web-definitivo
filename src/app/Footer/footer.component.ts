import { Component, OnInit } from '@angular/core';
import { ServicioService } from "../Providers/servicio.service";


@Component({
  selector: 'footer-themis',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class footer implements OnInit {
  footer: any[] =[];
  url : string;
  constructor(private footerprovider: ServicioService) {
    }
    ngOnInit() {
      this.footerprovider.getdatos('empresa').subscribe(res => {
          let info: any = res;
          this.footer = info.data;
          });
          
      }
      abrirface(url){
        this.url = url.facebook
        
        window.open(this.url, "_blank")
      }
      abririnsta(url){
        this.url = url.instagram
        window.open(this.url, "_blank")
      }
      abrirtwit(url){
        this.url = url.twitter
        window.open(this.url, "_blank")
      }
  }
