import { Component, OnInit } from '@angular/core';
import * as M from '../../assets/js/materialize.min.js';
import { ServicioService } from "../Providers/servicio.service";
@Component({
  selector: 'inicio-themis',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class home implements OnInit{

  options={indicators:false, interval:3000,height:800};
  carrusel: any[]= [];
  inicio: any[]= [];
  empleados: any[] = [];
  empleados2: any[] = [];
  imagen: any;
  tamano : number;
  col : number;
   constructor(private inicioprovider: ServicioService){
      this.carrusel=[];
      this.inicio=[];
      this.inicioprovider.getdatos('imagen_carrusel').subscribe(res => {
        let info: any = res;
        this.carrusel = info.data;
      });
   } 
  ngOnInit(){
    
    this.inicioprovider.getdatos('inicio_web').subscribe(res => {
      let info: any = res;
      this.inicio = info.data;
      var elems = document.querySelectorAll('.slider');
      var instances = M.Slider.init(elems, this.options);
    });
    this.inicioprovider.getdatos('vista_empleado/visibles').subscribe(res =>{
      let info: any = res;
      if(info.data.length<4){
        this.tamano = info.data.length
      }else{
      this.tamano = 4;
      }
      this.col = 12/this.tamano;

      for (let i = 0; i < this.tamano; i++){
        this.empleados.push({
          imagen: info.data[i].usuario_foto,
          nombre: info.data[i].empleado_nombre,
          apellido: info.data[i].empleado_apellido,
          rol: info.data[i].rol_usuario,
          especialidad: info.data[i].empleado_especialidad
        })   
      }
      this.empleados2 = this.empleados;

    })
    
  }

}


