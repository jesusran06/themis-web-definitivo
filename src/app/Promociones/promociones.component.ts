import { Component, OnInit } from '@angular/core';
import {FormBuilder} from '@angular/forms';
import { ServicioService } from "../Providers/servicio.service";
import * as moment from 'moment';
@Component({
  selector: 'promociones-themis',
  templateUrl: './promociones.component.html',
  styleUrls: ['./promociones.component.css']
})
export class promocion implements OnInit{
  promocion2: any[] = [];
  promocion: any[] = [];
  valor = '';
  ReiniciarPromocion() {
    this.promocion = this.promocion2;
  }

  constructor(private promoprovider: ServicioService) {
    this.promocion = []
  }

  ngOnInit() {
    this.promocion = [];
    this.promoprovider.getdatos('promocion').subscribe(res=>{
      let info: any = res;
      for (let i = 0; i < info.data.length; i++){
        
        
          this.promocion.push({
            imagen: info.data[i].imagen,
            nombre: info.data[i].nombre,
            descripcion: info.data[i].descripcion,
            fecha_inicio: moment(info.data[i].fecha_inicio).format("DD/MM/YYYY"),
            fecha_fin: moment(info.data[i].fecha_fin).format("DD/MM/YYYY")
          })                  
        
      }     
      this.promocion2 = this.promocion;
    });
    
  }
  FiltrarPromocionNombre() {
    console.log(this.valor)
    if (this.valor != "") {
      console.log(this.promocion2)
      this.promocion = this.promocion2
        .filter(x => x.nombre.trim()
          .toLowerCase().includes(this.valor.trim().toLowerCase()) == true);
    } else {
      this.ReiniciarPromocion();
    }
  }
  valorInput() {
    this.valor = '';
    this.ReiniciarPromocion();
  }

}