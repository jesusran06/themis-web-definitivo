import { Component, OnInit } from '@angular/core';
import { ServicioService } from "../Providers/servicio.service";


@Component({
  selector: 'servicios-themis',
  templateUrl: './servicios.component.html',
  styleUrls: ['./servicios.component.css']
})

export class servicio implements OnInit {

  valor = '';
  valor0 = '';
  servicios2: any[] = [];
  catalogo2: any[] = [];
 

  public servicios: any;
  public catalogo: any;
  servicios3: any [] = [];
  constructor(private servicioprovider: ServicioService) {
    this.servicios = [];
    this.catalogo = [];
  }

  ngOnInit() {
    this.servicioprovider.getdatos('categoria/visibles').subscribe(res => {
      let info: any = res;
      
      for (let i = 0; i < info.data.length; i++){
        
        if(info.data[i].estatus=='A'){
          this.servicios.push({
            id: info.data[i].id,
            nombre: info.data[i].nombre,
            descripcion: info.data[i].descripcion
          })                  
        }
      }

      this.servicios2 = this.servicios;
      
    });
  }

  OnClick(ser: any) {
    this.catalogo = [];
    this.catalogo2 = [];
    this.servicioprovider.getdatos('catalogo_servicio/categoria/'+ser.id+'/show').subscribe(res=>{
      let info: any = res;
      for (let i = 0; i < info.data.length; i++){
        
        if(info.data[i].estatus=='A'){
          this.catalogo.push({
            imagen: info.data[i].imagen,
            nombre: info.data[i].nombre,
            descripcion: info.data[i].descripcion
          })                  
        }
      }     
      this.catalogo2 = this.catalogo;
    })
    
  }


  FiltrarServiciosNombre() {

    if (this.valor != "") {
      this.servicios = this.servicios2
        .filter(x => x.nombre.trim()
          .toLowerCase().includes(this.valor.trim().toLowerCase()) == true);
    } else {
      this.ReiniciarServicios();
    }
  }

  FiltrarCategoriaNombre() {
    if (this.valor0 != "") {
      this.catalogo = this.catalogo2
        .filter(x => x.nombre.trim()
          .toLowerCase().includes(this.valor0.trim().toLowerCase()) == true);
    } else {
      this.ReiniciarCategoria();
    }
  }

  valorInput() {
    this.valor = '';
    this.ReiniciarServicios();
  }

  valorInput0() {
    this.valor0 = '';
    this.ReiniciarCategoria();
  }

  ReiniciarServicios() {
    this.servicios = this.servicios2;
  }

  ReiniciarCategoria() {
    this.catalogo = this.catalogo2;
  }
}