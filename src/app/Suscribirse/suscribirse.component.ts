import {Component, OnInit} from '@angular/core';
import {FormControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import { SuscribcionService } from "../Providers/suscribcion.service";
import * as moment from 'moment';
import { ServicioService } from '../Providers/servicio.service'
import { AppDateAdapter, APP_DATE_FORMATS} from './dateAdapter';
import { DateAdapter, NativeDateAdapter,MAT_DATE_FORMATS } from '@angular/material';
import { HttpClient } from '@angular/common/http';
/**
 * @title Stepper overview
 */
@Component({
  selector: 'pasos-themis',
  templateUrl: 'suscribirse.component.html',
  styleUrls: ['suscribirse.component.css'],
  providers: [
    {
        provide: DateAdapter, useClass: AppDateAdapter
    },
    {
        provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
    }
    ]
})
export class suscribirse implements OnInit {
  
  
  mensaje: string;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  opcion = 'Ninguno';
  existe : boolean;
  
  startDate = new Date(1990, 0, 1);
  constructor(private _formBuilder: FormBuilder, public cliente:SuscribcionService, public getcorreo: ServicioService, public http: HttpClient) {
    }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      nombrectrl : ['',
      Validators.required
    ],
    apellidoctrl : ['',
      Validators.required
    ],
    
    fechactrl : ['',
      Validators.required
    ],
    sexoctrl : ['',
      Validators.required
    ]
    });
    this.secondFormGroup = this._formBuilder.group({
      telefonoctrl : ['', 
      Validators.required],

      correoctrl : ['', 
      Validators.required]

    });
  }
   
  nombrectrl = new FormControl('',
    [
      Validators.required
    ]
    );
    apellidoctrl = new FormControl('',
    [
      Validators.required
    ]
    );  
    correoctrl = new FormControl('',
    [
      Validators.required
    ]
    );  
    fechactrl = new FormControl('',
    [
      Validators.required
    ]
    );  
    telefonoctrl = new FormControl('',
    [
      Validators.required
    ]
    );  
    sexoctrl = new FormControl('',
    [
      Validators.required
    ]
    );  
  
  onSubscribe(){
    const datos = new FormData();
    const email = new FormData();
    
    if(this.secondFormGroup.get('correoctrl').valid){
      email.append('correo', this.secondFormGroup.get('correoctrl').value)
    }
    if(this.firstFormGroup.get('nombrectrl').valid && this.firstFormGroup.get('apellidoctrl').valid && this.firstFormGroup.get('sexoctrl').valid && 
       this.firstFormGroup.get('fechactrl').valid && this.secondFormGroup.get('telefonoctrl').valid && this.secondFormGroup.get('correoctrl').valid){
        datos.append('nombre', this.firstFormGroup.get('nombrectrl').value);
        datos.append('apellido', this.firstFormGroup.get('apellidoctrl').value);
        datos.append('fecha_nac', moment(this.firstFormGroup.get('fechactrl').value).format("DD/MM/YYYY"));
        datos.append('correo', this.secondFormGroup.get('correoctrl').value);
        datos.append('telefono', this.secondFormGroup.get('telefonoctrl').value);
        datos.append('sexo', this.firstFormGroup.get('sexoctrl').value);
        console.log(this.firstFormGroup.get('sexoctrl').value)
      }
        return new Promise((resolve, reject) => {
          this.http.post('https://eos-themis.herokuapp.com/api/signup',email)
            .subscribe(res => {
              let info : any = res;
              this.existe = false;            
                this.mensaje = 'Ahora estas suscrito a nuestro sistema. Verifica tu correo y sigue las instrucciones.';             
            },(err) => {
              if(err.error.data.message=='Ya existe un usuario con ese correo'){
              this.existe = true;
              this.mensaje = 'Este usuario ya existe, por favor introduzca otro correo';
              }
            }
            )             
              
            });
          
  }
}