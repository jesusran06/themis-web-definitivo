
import { fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSidenavModule } from '@angular/material/sidenav';
import { NavMatComponent } from './nav-mat.component';

describe('NavMatComponent', () => {
  let component: NavMatComponent;
  let fixture: ComponentFixture<NavMatComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      imports: [MatSidenavModule],
      declarations: [NavMatComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NavMatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
