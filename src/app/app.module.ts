import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { MaterialAn } from "./AngularMaterial";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { servicio } from "./Servicios/servicios.component";
import { promocion } from "./Promociones/promociones.component";
import { home } from "./Inicio/inicio.component";
import { ConocenosComponent } from "./Conocenos/conocenos.component";

import { NavMatComponent } from "./nav-mat/nav-mat.component";
import { footer } from "./Footer/footer.component";
import { suscribirse } from "./Suscribirse/suscribirse.component";
import { nosotrosexport } from "./Nosotros/nosotros.component";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SuscribcionService } from "./Providers/suscribcion.service";
import { HttpClientModule } from '@angular/common/http';
import { Escuchath } from "./Escucha/escucha.component";
import { OwlDateTimeModule, OwlNativeDateTimeModule, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';

@NgModule({
  declarations: [
    AppComponent,
    NavMatComponent,
    footer,
    suscribirse,
    servicio,
    nosotrosexport,
    promocion,
    home,
    
    ConocenosComponent,
    Escuchath,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFontAwesomeModule,
    MaterialAn,
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    OwlDateTimeModule, 
    OwlNativeDateTimeModule,
    
  ],
  providers: [SuscribcionService, {provide: OWL_DATE_TIME_LOCALE, useValue: 'es'}],
  bootstrap: [AppComponent]
})
export class AppModule { }
