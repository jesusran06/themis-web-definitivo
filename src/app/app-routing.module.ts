import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { suscribirse } from "../app/Suscribirse/suscribirse.component";
import { servicio } from "../app/Servicios/servicios.component";
import { nosotrosexport } from "../app/Nosotros/nosotros.component";
import { promocion } from "../app/Promociones/promociones.component";
import { home } from "../app/Inicio/inicio.component";
import { ConocenosComponent } from "../app/Conocenos/conocenos.component";

const routes: Routes =[
     { path: 'inicio', component: home },
     { path: 'suscribirse', component: suscribirse },
     { path: 'servicio', component: servicio },
     { path: 'conocenos', component: ConocenosComponent },
     { path: 'nosotros', component: nosotrosexport },
     { path: 'promociones', component: promocion },
     
     { path: '**', component: home },
    
 ];

 @NgModule({
     imports: [RouterModule.forRoot(routes)],
     exports: [RouterModule]
 })

 export class AppRoutingModule {}