import { Component, OnInit } from '@angular/core';
import { ServicioService } from "../Providers/servicio.service";
@Component({
  selector: 'app-nosotros',
  templateUrl: './nosotros.component.html',
  styleUrls: ['./nosotros.component.css']
})
export class nosotrosexport implements OnInit {

  nosotros : any[]= [];
  constructor(private nosotrosprovider: ServicioService) {
   }

  ngOnInit() {
    this.nosotrosprovider.getdatos('filosofia').subscribe(res => {
        let info: any = res;
        console.log(info);
        this.nosotros = info.data;
        console.log(this.nosotros)
        });
    }
  }
