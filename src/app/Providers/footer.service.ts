import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ServicioService {
  
  constructor(public http: HttpClient) {
  }
  getdatos(){
    return this.http.get('https://EOS-THEMIS.herokuApp.com/api/empresa');
}

}