import { Component, OnInit} from '@angular/core';
import * as M from '../../assets/js/materialize.min.js';
import { FormControl, Validators, FormBuilder, FormGroup } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ServicioService } from "../Providers/servicio.service";
import * as moment from 'moment';
@Component({
    selector: 'escucha-themis',
    templateUrl: 'escucha.component.html',
    styleUrls: ['escucha.component.css'],
  })

export class Escuchath implements OnInit {
    formSugerencia : FormGroup
    constructor(public formBuilder: FormBuilder, public http: HttpClient, private escuchaprovider: ServicioService){
        
    }
    ngOnInit() {
        var elems = document.querySelectorAll('.modal');
    var instances = M.Modal.init(elems);
    this.escuchaprovider.getdatos('canal_escucha').subscribe(res => {
      let info: any = res;
      
      });
    }
    correoFormControl = new FormControl('',
    [
      Validators.required
    ]
    );
    mensajeFormControl = new FormControl('',
    [
      Validators.required
    ]
    );
  enviar(){
    const datos = new FormData();
    if(this.correoFormControl.valid && this.mensajeFormControl.valid){
        datos.append('correo_emisor', this.correoFormControl.value);
        datos.append('texto', this.mensajeFormControl.value);
        datos.append('fecha', moment().format('MM-DD-YYYY'));
        
      }
        return new Promise((resolve, reject) => {
          this.http.post('https://eos-themis.herokuapp.com/api/canal_escucha',datos)
            .subscribe(res => {
              resolve(res);
            }, (err) => {
              reject(err);
            });
        });
      
  }
}